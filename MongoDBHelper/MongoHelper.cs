﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using MongoDB.Driver.Builders;
namespace MongoDBHelper
{
    public class MongoHelper
    {
        private MongoDatabase Matrix_Communication_DB;
        public MongoHelper(string Connection, string DBName)
        {
            var connectionString = Connection;
            var client = new MongoClient(connectionString);
            var server = client.GetServer();
            //server = MongoServer.Create();
            Matrix_Communication_DB = server.GetDatabase(DBName);
        }
        public int InsertData <T>(T objData,string CollectionTable)
        {
            try
            {                                
                var EmailCollection = Matrix_Communication_DB.GetCollection<T>(CollectionTable);                
                WriteConcernResult objresult= EmailCollection.Insert(objData);
                if (objresult.Ok == true)
                    return 1;
                else
                    return 0;
            }
            catch (MongoConnectionException MCEX)
            {
                throw MCEX; // Connection Exception
            }
            catch (Exception ex)
            {
               throw ex;
            }
        }
        public int BulkDataInsert<T>(List<T> objData, string CollectionTable)
        {
            try
            {
                var EmailCollection = Matrix_Communication_DB.GetCollection<T>(CollectionTable);
                EmailCollection.InsertBatch(objData);
                return 1;
                //if (objresult.Ok == true)
                //    return 1;
                //else
                //    return 0;
            }
            catch (MongoConnectionException MCEX)
            {
                throw MCEX; // Connection Exception
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<T> FindAllDocument<T>(string CollectionTable) where T : class
        {
            try
            {
                var EmailCollection = Matrix_Communication_DB.GetCollection<T>(CollectionTable);
                MongoCursor<T> cursor = EmailCollection.FindAllAs<T>();                
                return cursor.ToList();
            }
            catch (Exception ex)
            {
               throw ex;
            }
        }
        public int UpdateDocument(IMongoQuery query, IMongoUpdate update, string CollectionTable)
        {
            try
            {
                var EmailCollection = Matrix_Communication_DB.GetCollection(CollectionTable);
                WriteConcernResult objresult = EmailCollection.Update(query, update);
                if (objresult.Ok == true)
                    return 1;
                else
                    return 0;
            }
            catch (MongoConnectionException MCEX)
            {
                throw MCEX; // Connection Exception
            }
            catch (Exception ex)
            {
                throw ex;
            }            
        }
        public int Delete_ColectionByQyery<T>(IMongoQuery query, string CollectionTable)
        {
            try
            {
                var EmailCollection = Matrix_Communication_DB.GetCollection<T>("Email_Collection");
                long PreviousCount = EmailCollection.Count();
                EmailCollection.Remove(query);
                if (PreviousCount < EmailCollection.Count())
                    return 1;
                else
                    return 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int Delete_Colection<T>()
        {
            var EmailCollection = Matrix_Communication_DB.GetCollection<T>("Email_Collection");            
            EmailCollection.RemoveAll();
            return 0;
        }
        public List<T> GetInbox<T>(IMongoQuery query, string CollectionTable, IMongoSortBy strOrderField, IMongoFields Fields,int skip,int limit) where T : class
        {
            try
            {                
                if (limit == 0)
                {
                    limit = 1000;
                }
                var EmailCollection = Matrix_Communication_DB.GetCollection<T>(CollectionTable).Find(query).SetFields(Fields);

                if (skip == 0)
                    return EmailCollection.SetLimit(limit).ToList();
                else
                    return EmailCollection.Skip(skip).Take(limit).ToList();
            }
            catch (MongoConnectionException MCEX)
            {
                throw MCEX;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }        
        public T GetEmailDetails<T>(IMongoQuery query, string CollectionTable) where T : new()
        {
            try
            {
                var EmailCollection = Matrix_Communication_DB.GetCollection<T>(CollectionTable);
                return EmailCollection.FindOne(query);
            }
            catch (MongoConnectionException MCEX)
            {
                throw MCEX;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetSclarField(IMongoQuery query, string CollectionTable,string SelectField)
        {
            try
            {
                var EmailCollection = Matrix_Communication_DB.GetCollection(CollectionTable);
                BsonDocument bsonDoc = EmailCollection.FindOne(query);
                if (bsonDoc.Count() > 0)
                    return Convert.ToString(bsonDoc[SelectField]);
                else
                    return string.Empty;
            }
            catch (MongoConnectionException MCEX)
            {
                throw MCEX;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

