﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using MongoDB.Driver.Builders;


namespace OneLeadArchiveApp
{
    class Program
    {
        static void Main(string[] args)
        {
            SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["ConnectionString"]);

            con.Open();
            string strQueryInline = "select * from MTX.AgentQueue With(NOLOCK)";
            SqlDataAdapter adpt = new SqlDataAdapter(strQueryInline, con);
            DataTable dt = new DataTable();
            adpt.Fill(dt);

            var mongoConStr = "mongodb://localhost";
            var client = new MongoClient(mongoConStr);
            var server = client.GetServer();

            //server = MongoServer.Create();

            MongoDatabase _mongoDB = server.GetDatabase("OneLead_DB");
           
            var agentQueueCollection = _mongoDB.GetCollection<BsonDocument>("AgentQueueCollection");

            try
            {
                //for (int i = 0; i < 100; i++)
                //{
                //    BsonDocument bson = new BsonDocument();
                //    //bson.Add(dr.Table.Columns[i].ColumnName, dr[i].ToString());
                //    bson.Add("TestCol" + i, "testVal" + i);
                //    testCollection.Insert(bson);
                //}


                foreach (DataRow dr in dt.Rows)
                {
                    BsonDocument bson = new BsonDocument();

                    for (int i = 0; i < dr.ItemArray.Count(); i++)
                    {
                        bson.Add(dr.Table.Columns[i].ColumnName, dr[i].ToString());
                    }
                    agentQueueCollection.Insert(bson);
                }
            }
            catch (Exception ex)
            {

            }

        }
    }
}
